import argparse

import requests
from time import sleep

class APIPlatform:
    maptracker_url = "http://localhost"
    maptracker_port = 8080

    def __init__(self, maptracker_url="http://localhost", maptracker_port=8080):
        self.maptracker_url = maptracker_url
        self.maptracker_port = maptracker_port
        pass

    def post_data(self, platform_key, data, data_type="platform"):
        return post_data(platform_key, data, data_type=data_type, maptracker_url=self.maptracker_url, maptracker_port=self.maptracker_port)

    def update_platform_data(self, platform_key, data):
        response = self.post_data(platform_key, data, data_type="platform")
        return response, data

    def update_platform_mission(self, platform_key, mission_data):
        response = self.post_data(platform_key, mission_data, data_type="mission")
        return response, mission_data

    def log_platform_message(self, platform_key, msg_type, msg):
        msg_data = {"msg": msg, "type": msg_type}
        response = self.post_data(platform_key, msg_data, data_type="log")
        return response, msg_data

    def serve_forever(self):
        while True:
            sleep(1)


def post_data(platform_name, data, data_type="platform", maptracker_url="http://localhost", maptracker_port=8080):
    """
    Perform http post to maptracker API
    :param
    """
    resp = None
    try:
        resp = requests.post("{}:{}/data/platformdata.api.Platform/{}?data_type={}".format(
            maptracker_url, maptracker_port, platform_name, data_type), json=data
        )
        # Debug print
        print ("Data type: {data_type}, Message: {resp[msg]} ({resp[response]},{code}))".format(
            resp=resp.json(), code=resp.status_code, data_type=data_type)
        )
    except requests.exceptions.ConnectionError as ce:
        print ("*** ERROR: unable to connect to maptracker server: {}:{}".format(maptracker_url, maptracker_port))
        print ("*** PRINTING OFFLINE DEBUG INFO:")
        print ("    PLATFORM: {}\n    DATATYPE: {}\n    DATA: {}".format(platform_name, data_type, data))
    return resp


def get_args():
    # Parse input args
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", default=8080, type=int, help="Port for maptracker server")
    parser.add_argument("-u", "--url", default="http://localhost", type=str, help="URL for maptracker server (format: http://url)")
    args = parser.parse_args()

    return {"url": args.url, "port": args.port}